package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strings"
)

const (
	quitMessage = "quit"
)

func main() {
	// Connect to the server.
	conn, err := net.Dial("tcp", "127.0.0.1:4800")
	handleError(err)
	defer conn.Close()
	// Read user's input via buffered reader.
	stdinReader := bufio.NewReader(os.Stdin)

	for {
		// Get what user entered.
		text, err := stdinReader.ReadString('\n')
		handleError(err)
		text = strings.TrimSpace(text)

		// Send it to the server.
		fmt.Fprintf(conn, text+"\n")

		// If it's a quit message,
		// interrupt the loop.
		if text == quitMessage {
			break
		}

		// Get the server response and output it.
		message, err := bufio.NewReader(conn).ReadString('\n')
		handleError(err)
		fmt.Println("Server responded:", strings.TrimSpace(message))
	}
}

func handleError(err error) {
	if err != nil {
		panic(err)
	}
}
