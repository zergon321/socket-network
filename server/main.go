package main

import (
	"bufio"
	"fmt"
	"net"
	"strings"
)

const (
	quitMessage = "quit"
)

func main() {
	// Listen for client connections.
	listener, err := net.Listen("tcp", ":4800")
	handleError(err)
	defer listener.Close()

	// Accept the client connection.
	conn, err := listener.Accept()
	handleError(err)
	defer conn.Close()

	for {
		// Read the message from the client.
		message, err := bufio.NewReader(conn).ReadString('\n')
		handleError(err)
		message = strings.TrimSpace(message)

		// If quit message is received,
		// disconnect from the client.
		if message == quitMessage {
			break
		}

		fmt.Println("Message received:", message)

		// Form a reply to the client message.
		reply := strings.ToUpper(message)
		conn.Write([]byte(reply + "\n"))
	}
}

func handleError(err error) {
	if err != nil {
		panic(err)
	}
}
